Collections
===========

Working through the book [Learn Rust With Entirely Too Many Linked Lists][1].

This crate contains a couple implementations of linked lists from the book 
and a handful of other collections I implemented to learn Rust.

[1]: https://rust-unofficial.github.io/too-many-lists/index.html
