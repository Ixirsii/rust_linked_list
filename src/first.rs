use std::mem;

/* ******************************************* Types ******************************************** */

/// A bad stack.
#[derive(Debug)]
pub struct List<T> {
    head: Link<T>,
}

/// Link to the next [Node] in a [List].
#[derive(Debug)]
enum Link<T> {
    Empty,
    More(Box<Node<T>>)
}

/// A node in a [List] containing an element and a link to the next node.
#[derive(Debug)]
struct Node<T> {
    elem: T,
    next: Link<T>,
}

/* *************************************** Implementation *************************************** */

impl<T> List<T> {
    /// Construct an empty list.
    pub fn new() -> Self {
        List { head: Link::Empty }
    }

    /// Pop the most recent element off the stack.
    pub fn pop(&mut self) -> Option<T> {
        match mem::replace(&mut self.head, Link::Empty) {
            Link::Empty => None,
            Link::More(node) => {
                self.head = node.next;
                Some(node.elem)
            }
        }
    }

    /// Push an element onto the stack.
    pub fn push(&mut self, elem: T) {
        let new_node = Box::new(Node {
            elem,
            next: mem::replace(&mut self.head, Link::Empty)
        });

        self.head = Link::More(new_node)
    }
}

impl<T> Drop for List<T> {
    fn drop(&mut self) {
        let mut cur_link = mem::replace(&mut self.head, Link::Empty);

        while let Link::More(mut boxed_node) = cur_link {
            cur_link = mem::replace(&mut boxed_node.next, Link::Empty);
            // boxed_node goes out of scope and gets dropped here;
            // but its Node's `next` field has been set to Link::Empty
            // so no unbounded recursion occurs.
        }
    }
}

/* ******************************************* Tests ******************************************** */

#[cfg(test)]
mod test {
    use super::List;

    #[test]
    fn given_empty_list_when_pop_then_returns_none() {
        // Given
        let mut list: List<i32> = List::new();

        // When
        let actual: Option<i32> = list.pop();

        // Then
        assert_eq!(actual, None);
    }

    #[test]
    fn given_list_when_pop_then_returns_tail() {
        // Given
        let expected: i32 = 1;
        let mut list: List<i32> = List::new();

        list.push(expected);

        // When
        let actual: Option<i32> = list.pop();

        // Then
        assert_eq!(actual, Some(expected));
    }

    #[test]
    fn given_modified_list_when_pop_then_returns_tail() {
        // Given
        let expected: i32 = 2;
        let mut list: List<i32> = List::new();

        list.push(1);
        list.pop();
        list.push(expected);

        // When
        let actual: Option<i32> = list.pop();

        assert_eq!(actual, Some(expected));
    }

    #[test]
    fn given_list_when_pop_then_returns_none_eventually() {
        // Given
        let mut list: List<i32> = List::new();

        list.push(1);
        list.pop();

        // When
        let actual: Option<i32> = list.pop();

        // Then
        assert_eq!(actual, None);
    }
}
