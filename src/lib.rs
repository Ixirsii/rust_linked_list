//! This crate is a handful of useless collection implemented so that I can learn Rust.

#![deny(rustdoc::broken_intra_doc_links)]
#![warn(missing_docs)]
#![warn(rustdoc::missing_crate_level_docs)]

/// A bad stack
pub mod first;

/// An OK stack
pub mod second;

/// A Persistent stack
pub mod third;
