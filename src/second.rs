/* ******************************************* Types ******************************************** */

/// Implement `into_iter` for a [List].
#[derive(Debug)]
pub struct IntoIter<T>(List<T>);

/// Implement `iter` for a [List].
#[derive(Debug)]
pub struct Iter<'a, T> {
    next: Option<&'a Node<T>>,
}

/// Implement `iter_mut` for a [List].
#[derive(Debug)]
pub struct IterMut<'a, T> {
    next: Option<&'a mut Node<T>>,
}

/// An OK stack.
#[derive(Debug)]
pub struct List<T> {
    head: Link<T>,
}

/// An [Option]al link to the next [Node] in a [List].
type Link<T> = Option<Box<Node<T>>>;

/// A node in a [List] with a [Link] to the next node.
#[derive(Debug)]
struct Node<T> {
    elem: T,
    next: Link<T>,
}

/* *************************************** Implementation *************************************** */

impl<T> List<T> {
    /// Construct an empty list.
    pub fn new() -> Self {
        List { head: None }
    }

    /// Wrap the list in an iterable structure.
    pub fn into_iter(self) -> IntoIter<T> {
        IntoIter(self)
    }

    /// Iterate through the list.
    pub fn iter(&self) -> Iter<T> {
        Iter { next: self.head.as_deref().map(|node: &Node<T>| &*node) }
    }

    /// Iterate through the list allowing the elements to be mutated.
    pub fn iter_mut(&mut self) -> IterMut<T> {
        IterMut { next: self.head.as_deref_mut() }
    }

    /// Peek at the head of the list without removing it.
    pub fn peek(&self) -> Option<&T> {
        self.head.as_ref().map(|node: &Box<Node<T>>| {
            &node.elem
        })
    }

    /// Get and remove the head of the list.
    pub fn pop(&mut self) -> Option<T> {
        self.head.take().map(|node: Box<Node<T>>| {
            self.head = node.next;
            node.elem
        })
    }

    /// Push an element onto the head of the list.
    pub fn push(&mut self, elem: T) {
        let new_node = Box::new(Node {
            elem,
            next: self.head.take(),
        });

        self.head = Some(new_node)
    }
}

impl<T> Drop for List<T> {
    fn drop(&mut self) {
        let mut cur_link: Option<Box<Node<T>>> = self.head.take();

        while let Some(mut boxed_node) = cur_link {
            cur_link = boxed_node.next.take();
            // boxed_node goes out of scope and gets dropped here;
            // but its Node's `next` field has been set to Link::Empty
            // so no unbounded recursion occurs.
        }
    }
}

impl<T> Iterator for IntoIter<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.pop()
    }
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = &'a T;

    fn next(&mut self) -> Option<Self::Item> {
        self.next.take().map(|node: &Node<T>| {
            self.next = node.next.as_deref().map(|node: &Node<T>| &*node);
            &node.elem
        })
    }
}

impl<'a, T> Iterator for IterMut<'a, T> {
    type Item = &'a mut T;

    fn next(&mut self) -> Option<Self::Item> {
        self.next.take().map(|node: &mut Node<T>| {
            self.next = node.next.as_deref_mut();
            &mut node.elem
        })
    }
}

/* ******************************************* Tests ******************************************** */

#[cfg(test)]
mod test {
    use super::IntoIter;
    use super::Iter;
    use super::IterMut;
    use super::List;

    #[test]
    fn given_list_when_into_iter_then_can_iterate() {
        // Given
        let expected: i32 = 1;
        let mut list: List<i32> = List::new();

        list.push(expected);

        // When
        let mut iter: IntoIter<i32> = list.into_iter();

        // Then
        assert_eq!(iter.next(), Some(expected));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn given_list_when_iter_then_can_iterate() {
        // Given
        let expected: i32 = 1;
        let mut list: List<i32> = List::new();

        list.push(expected);

        // When
        let mut iter: Iter<i32> = list.iter();

        // Then
        assert_eq!(iter.next(), Some(&expected));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn given_list_when_iter_mut_then_can_iterate() {
        // Given
        let mut list: List<i32> = List::new();

        list.push(1);

        // When
        let mut iter: IterMut<i32> = list.iter_mut();

        // Then
        assert_eq!(iter.next(), Some(&mut 1));
        assert_eq!(iter.next(), None);
    }

    #[test]
    fn given_empty_list_when_peek_then_returns_none() {
        // Given
        let list: List<i32> = List::new();

        // When
        let actual: Option<&i32> = list.peek();

        // Then
        assert_eq!(actual, None);
    }

    #[test]
    fn given_list_when_peek_then_returns_reference() {
        // Given
        let mut list: List<i32> = List::new();

        list.push(1);
        list.push(2);
        list.push(3);

        // When
        let actual: Option<&i32> = list.peek();

        // Then
        assert_eq!(actual, Some(&3));
    }

    #[test]
    fn given_empty_list_when_pop_then_returns_none() {
        // Given
        let mut list: List<i32> = List::new();

        // When
        let actual: Option<i32> = list.pop();

        // Then
        assert_eq!(actual, None);
    }

    #[test]
    fn given_list_when_pop_then_returns_tail() {
        // Given
        let expected: i32 = 1;
        let mut list: List<i32> = List::new();

        list.push(expected);

        // When
        let actual: Option<i32> = list.pop();

        // Then
        assert_eq!(actual, Some(expected));
    }

    #[test]
    fn given_modified_list_when_pop_then_returns_tail() {
        // Given
        let expected: i32 = 2;
        let mut list: List<i32> = List::new();

        list.push(1);
        list.pop();
        list.push(expected);

        // When
        let actual: Option<i32> = list.pop();

        assert_eq!(actual, Some(expected));
    }

    #[test]
    fn given_list_when_pop_then_returns_none_eventually() {
        // Given
        let mut list: List<i32> = List::new();

        list.push(1);
        list.pop();

        // When
        let actual: Option<i32> = list.pop();

        // Then
        assert_eq!(actual, None);
    }
}
