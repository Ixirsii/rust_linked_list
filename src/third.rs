use std::sync::Arc;

/* ******************************************* Types ******************************************** */

/// Linked list of shared pointers.
#[derive(Debug)]
pub struct List<T> {
    head: Link<T>,
}

/// Link to the next node in the list.
type Link<T> = Option<Arc<Node<T>>>;

/// Node in a list containing the element and link to the next node.
#[derive(Debug)]
struct Node<T> {
    elem: T,
    next: Link<T>,
}

/* *************************************** Implementation *************************************** */

impl<T> List<T> {
    /// Construct an empty list.
    pub fn new() -> Self {
        List { head: None }
    }

    /// Get the head of the list.
    pub fn head(&self) -> Option<&T> {
        self.head.as_ref().map(|node: &Arc<Node<T>>| &node.elem)
    }

    /// Prepend an element to the front of the list.
    pub fn prepend(&self, elem: T) -> List<T> {
        List {
            head: Some(Arc::new(Node {
                elem,
                next: self.head.clone(),
            }))
        }
    }

    /// Get all elements in the list except for the head.
    pub fn tail(&self) -> List<T> {
        List { head: self.head.as_ref().and_then(|node: &Arc<Node<T>>| node.next.clone()) }
    }
}

impl<T> Drop for List<T> {
    fn drop(&mut self) {
        let mut head: Option<Arc<Node<T>>> = self.head.take();

        while let Some(node) = head {
            if let Ok(mut node) = Arc::try_unwrap(node) {
                head = node.next.take();
            } else {
                break;
            }
        }
    }
}

/* ******************************************* Tests ******************************************** */

#[cfg(test)]
mod test {
    use super::List;

    #[test]
    fn given_empty_list_when_head_then_returns_none() {
        // Given
        let list: List<i32> = List::new();

        // When
        let actual: Option<&i32> = list.head();

        // Then
        assert_eq!(actual, None);
    }

    #[test]
    fn given_list_when_head_then_returns_head() {
        // Given
        let expected: i32 = 1;
        let list: List<i32> = List::new()
            .prepend(expected);

        // When
        let actual: Option<&i32> = list.head();


        // Then
        assert_eq!(actual, Some(&expected));
    }

    #[test]
    fn given_empty_list_when_tail_then_returns_empty() {
        // Given
        let list: List<i32> = List::new();

        // When
        let actual: List<i32> = list.tail();

        // Then
        assert_eq!(actual.head(), None);
    }

    #[test]
    fn given_list_when_tail_then_returns_tail() {
        // Given
        let expected: i32 = 1;
        let list: List<i32> = List::new()
            .prepend(expected)
            .prepend(2);

        // When
        let actual: List<i32> = list.tail();

        // Then
        assert_eq!(actual.head(), Some(&expected));
    }
}
